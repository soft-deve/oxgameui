/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuntapong.oxgameui;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Table implements Serializable{

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private int row, col;
    private Player win;
    private boolean finish = false;
    private int round = 0;
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + " ");
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }
    public char getRowCol(int row,int col){
        return table[row][col];
    }
    public boolean setRowCol(int row, int col) {
        if(isFinish()) return false;
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.row = row;
            this.col = col;
            round ++;
            CheckWin();
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
        setStatWinlose();
    }
        private void setStatWinlose(){
            if(currentPlayer == playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
        }
    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[row][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        win = currentPlayer;
         setStatWinlose();
    }

    void checkX() {
        if (table[0][0] == table[1][1] && table[1][1] == table[2][2]
                && table[0][0] != '-') {
            finish = true;
            win = currentPlayer;
            setStatWinlose();
        }
        if (table[0][2] == table[1][1] && table[1][1] == table[2][0]
                && table[0][2] != '-') {
            finish = true;
            win = currentPlayer;
            setStatWinlose();
        }
        return;
    }
    void checkDraw() {
        if (round != 9) {
            return;
        }
        finish = true;
        playerO.draw();
        playerX.draw();
    }

    public void CheckWin() {
        checkRow();
        checkCol();
        checkX();
        checkDraw();
    }
    public boolean isFinish(){
        return finish;
    }
    public Player getWin(){
        return win;
    }
}
